all: clear build install

build:
	valac --pkg gtk+-3.0 --pkg webkit2gtk-4.0 --pkg posix webkit.vala -o telegram

install:
	install telegram $(DESTDIR)/usr/bin/telegram
	install telegram.desktop $(DESTDIR)/usr/share/applications/telegram.desktop
	install telegram-react.png $(DESTDIR)/usr/share/icons/telegram-react.png

clear:
	rm -f telegram

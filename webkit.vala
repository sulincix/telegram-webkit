using Gtk;
using WebKit;
using Posix;

public bool d=true;
public class ValaBrowser : Window {

    private const string TITLE = "Telegram";
    private const string DEFAULT_URL = "https://evgeny-nadymov.github.io/telegram-react/";
    private const string DEFAULT_PROTOCOL = "https";

    private Regex protocol_regex;

    private WebView web_view;

    public ValaBrowser () {
        this.title = ValaBrowser.TITLE;
        set_default_size (1000, 600);

        try {
            this.protocol_regex = new Regex (".*://.*");
        } catch (RegexError e) {
            critical ("%s", e.message);
        }

        create_widgets ();
        connect_signals ();
    }

    private void create_widgets () {
        this.web_view = new WebView ();
        var scrolled_window = new ScrolledWindow (null, null);
        scrolled_window.set_policy (PolicyType.AUTOMATIC, PolicyType.AUTOMATIC);
        scrolled_window.add (this.web_view);
        var box = new Box (Gtk.Orientation.VERTICAL, 0);
        box.pack_start (scrolled_window, true, true, 0);
        add (box);
	icon=new Gdk.Pixbuf.from_file ("/usr/share/icons/telegram-react.png");
    }

    private void connect_signals () {
        this.destroy.connect (()=>{
		this.destroy();
		d=false;
	});
	this.web_view.load_changed.connect ((source, evt) => {
		if(!(DEFAULT_URL in source.get_uri())){
		Posix.system("xdg-open "+source.get_uri ());
		}
        });

    }


    public void start () {
        show_all ();
        this.web_view.load_uri (DEFAULT_URL);
    }

    public static int main (string[] args) {
        Gtk.init (ref args);
	var trayicon = new StatusIcon.from_file ("/usr/share/icons/telegram-react.png");
        var browser = new ValaBrowser ();
        browser.start ();
	trayicon.activate.connect(()=>{
		if(!d){
			browser = new ValaBrowser ();
	        	browser.start ();
		}
	});

        Gtk.main ();

        return 0;
    }
}
